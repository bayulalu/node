var fs = require('fs');
var url = require('url');



function renderFile(fileName, res){
    res.writeHead(200, {'Content-Type' : 'text/html'});
    fs.readFile(fileName, null, function(err, data){
        if (err) {
            res.writeHead(404);
            res.write('file tidak ditemukan');
        }else{
           res.write(data) 
        }1

        res.end();
    });
}

module.exports = {
    hendleRequest : function(req, res) {
        res.writeHead(200, {'Content-Type' : 'text/html'});
        var path = url.parse(req.url).pathname;

        switch (path) {
            case '/':
                renderFile('./index.html', res)
                break;
            case '/user':
                renderFile('./user.html', res)
                break;
            default:
                break;
        }
    }
};
